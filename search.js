
// deno run --allow-env --allow-net search.js k33g
let gitLabToken = Deno.env.get("GITLAB_TOKEN_ADMIN")
let gitLabUrl = "https://gitlab.com/api/v4"

let searchString = Deno.args[0]

async function getData(perPage, page) {
  let url = `${gitLabUrl}/search?scope=projects&search=${searchString}&per_page=${perPage}&page=${page}`
  let response = await fetch(url,{
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Private-Token": gitLabToken
    }
  })
  //console.log(response.status)
  //console.log(response.statusText)
  let jsonData = await response.json()
  return jsonData
}

var data = []
var page = 1
let perPage = 100
do {
  data = await getData(perPage,page)
  data.forEach(element => {
    console.log(element.id, element.path_with_namespace)
  })
  page+=1
} while (data.length>0)

/*
global search scope project
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=projects&search=flight"


group search
 GET /groups/:id/search
 scope project curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=projects&search=flight"

 https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
 look at JayBot
*/

